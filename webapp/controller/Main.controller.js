sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "alterno/zreqbudgetreport/model/formatter",
    "alterno/zreqbudgetreport/model/popovers",
    "sap/ui/export/Spreadsheet",
    "sap/ui/table/TablePersoController",
    "sap/m/MessageBox"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, Formatters, Popovers, Spreadsheet, TablePersoController, MessageBox) {
        "use strict";

        return Controller.extend("alterno.zreqbudgetreport.controller.Main", {
            popover: Popovers,
            formatter: Formatters,

            onInit: function () {

                owner._mainController = this;
                owner._mainView = this.getView();

                var that = this;
                // owner.getModel().metadataLoaded().then(function () {

                oModels.getUserArea().then(function (data) {
                    oModels.loadProjects();
                    oModels.loadCommitments();

                });
            // }

            },
            resetFilters: function () {
                var userInfo = new sap.ushell.services.UserInfo().getUser(),
                    model = owner.getModel("JSON"),
                    oDateFrom = new Date(),
                    defaultFilters = {};

                //one month ago
                // oDateFrom.setFullYear(new Date(new Date().setDate(new Date().getDate() - 30)).getFullYear());
                // oDateFrom.setMonth(new Date(new Date().setDate(new Date().getDate() - 30)).getMonth());
                // oDateFrom.setDate(new Date(new Date().setDate(new Date().getDate() - 30)).getDate());
                oDateFrom = new Date("01/01/" + oDateFrom.getFullYear().toString());

                defaultFilters = {
                    productionDateFrom: oDateFrom,
                    productionDateTo: new Date(),
                    requestCreated: userInfo.getId(),
                    requestCreatedValue: userInfo.getFullName(),
                    showBudgetIndex: "1",
                    onlyOpened: true,
                    requestType: "",
                    requestStatus: "2",
                    financialYear: "",
                    teamKey: "",
                    teamValue: "",
                    project: "",
                    projectValue: "",
                    commitment: "",
                    commitmentValue: "",
                    mahlakaValue: "",
                    stnValue: ""
                };

                model.setProperty("/selectedFilters", defaultFilters);

                //owner._mainController.resetUiTableFilters();
                owner.getModel("JSON").refresh(false)
            },

            filterData: function (event) {
                var model = owner.getModel("JSON"),
                    fData = model.getProperty("/selectedFilters"),
                    oFilters = [],
                    projectType = (Number(fData.showBudgetIndex) + 1).toString();

                oFilters.push(new sap.ui.model.Filter("IvCourse", "EQ", projectType));
                oFilters.push(new sap.ui.model.Filter("IvAddGrps", "EQ", true));

                if (fData.onlyOpened) {
                    oFilters.push(new sap.ui.model.Filter("IvMyDocs", "EQ", true));
                }
                if (fData.freeText) {
                    oFilters.push(new sap.ui.model.Filter("IvDocnr", "EQ", fData.freeText));
                    oFilters.push(new sap.ui.model.Filter("IvExpl", "EQ", fData.freeText));
                    oFilters.push(new sap.ui.model.Filter("IvFundsctrSf", "EQ", fData.freeText));
                }
                if (fData.requestType) {
                    oFilters.push(new sap.ui.model.Filter("IvBudtype", "EQ", fData.requestType));
                }
                if (fData.requestStatus && fData.requestStatus !== "5") {
                    oFilters.push(new sap.ui.model.Filter("IvDocstate", "EQ", fData.requestStatus));
                }
                if (fData.financialYear) {
                    oFilters.push(new sap.ui.model.Filter("IvDocyear", "EQ", fData.financialYear));
                }
                if (fData.project) {
                    oFilters.push(new sap.ui.model.Filter("IvFundsctr", "EQ", fData.project));
                }
                if (fData.commitment) {
                    oFilters.push(new sap.ui.model.Filter("IvCmmtitem", "EQ", fData.commitment));
                }
                if (fData.teamKey) {
                    oFilters.push(new sap.ui.model.Filter("IvIdnum", "EQ", fData.teamKey));
                }
                if (fData.requestCreated) {
                    oFilters.push(new sap.ui.model.Filter("IvCrtuser", "EQ", fData.requestCreated));
                }
                if (fData.mahlakaValue) {
                    oFilters.push(new sap.ui.model.Filter("Mahlaka", "EQ", fData.mahlakaValue));
                }
                if (fData.stnValue) {
                    oFilters.push(new sap.ui.model.Filter("Stn", "EQ", fData.stnValue));
                }
                if (fData.productionDateFrom || fData.productionDateTo) {
                    var date1 = fData.productionDateFrom ? this.getFixedDate(fData.productionDateFrom) : undefined,
                        date2 = fData.productionDateTo ? this.getFixedDate(fData.productionDateTo) : undefined;
                    if (date1 && date2) {
                        oFilters.push(new sap.ui.model.Filter("IrCrtDate", "BT", date1, date2));
                    } else if (date1) {
                        oFilters.push(new sap.ui.model.Filter("IrCrtDate", "GE", date1));
                    } else if (date2) {
                        oFilters.push(new sap.ui.model.Filter("IrCrtDate", "LE", date2));
                    }
                }
                oModels.getReqTable(oFilters);

            },
            resetUiTableFilters: function () {
                var table = owner._mainController.byId("reqTable"),
                    columns = owner._mainController.byId("reqTable").getColumns();
                columns[0].setSortOrder();
                for (var i = 0; i < columns.length; i++) {
                    columns[i].setFilterValue("");
                    columns[i].setFiltered();
                    columns[i].setSorted();
                }
                table.getBinding("rows").filter();

            },
            getFixedDate: function (value) {
                if (!value) {
                    return;
                }
                var before = value.getTime();
                var after = new Date(before + 10800000); //  adding three hours to fix UTC gateway processing day back
                return after;
            },
            onExport: function () {
                var aCols, aItems, oSettings, oSheet, aRows, aReqTable;

                aCols = this.createColumnConfig();
                aItems = owner.getModel("JSON");

                aReqTable = owner.getModel("JSON").getProperty("/reqTable");
                aRows = [];
                for (var i = 0; i < aReqTable.length; i++) {
                    aReqTable[i]['IsParent']  = 'X';
                    aRows.push(aReqTable[i]);

                    if(!!aReqTable[i]['sons']){
                        for (var j = 0; j < aReqTable[i]['sons'].length; j++) {
                            aReqTable[i]['sons'][j]['IsParent']  = '';
                            aRows.push(aReqTable[i]['sons'][j]);
                        }
                    }
                }

                oSettings = {
                    workbook: {
                        columns: aCols
                    },
                    dataSource: aRows,
                    fileName: owner.getModel("JSON").getProperty("/ExportName")
                };

                oSheet = new Spreadsheet(oSettings);
                oSheet.build()
                    .then(function () { });

            },
            createColumnConfig: function () {
                var aColumns = [];

                aColumns.push({
                    label: owner.i18n('IsParent'),
                    property: 'IsParent'
                });
                aColumns.push({
                    label: owner.i18n('Docnr'),
                    property: 'Docnr',
                    width: '15rem'
                });
                aColumns.push({
                    label: owner.i18n('FINANCIAL_YEAR'),
                    property: 'Docyear',
                    width: '15rem'
                });
                aColumns.push({
                    label: owner.i18n('Status'),
                    property: 'Docstate',
                    width: '15rem'
                });
                aColumns.push({
                    label:  owner.i18n('REQUEST_CREATING'),
                    property: 'NameText',
                    width: '15rem'
                });
                aColumns.push({
                    label:  owner.i18n('ReqEss'),
                    property: 'Text',
                    width: '15rem'
                });
                aColumns.push({
                    label:  owner.i18n('ReqSum'),
                    property: 'DocSum',
                    width: '15rem'
                });
                aColumns.push({
                    label:  owner.i18n('FundsCtr'),
                    property: ['Bezeich', 'Fundsctr'],
                    template: '{0} - {1}',
                    width: '15rem'
                });
                aColumns.push({
                    label:  owner.i18n('FmArea'),
                    property: 'FmArea'
                });
                aColumns.push({
                    label:  owner.i18n('Waers'),
                    property: 'Waers',
                    width: '15rem'
                });
                aColumns.push({
                    label:  owner.i18n('department'),
                    property: 'Mahlaka',
                    width: '15rem'
                });
                aColumns.push({
                    label:  owner.i18n('station'),
                    property: 'Stn',
                    width: '15rem'
                });
                return aColumns;
        
            },
            navToReq: function (event) {
                var Docnr = event.getSource().getBindingContext("JSON").getProperty("Docnr"),
                    sYear = event.getSource().getBindingContext("JSON").getProperty("Docyear"),
                    sFmArea = event.getSource().getBindingContext("JSON").getProperty("FmArea");
                //     href = location.href,
                //     hrefNew = location.href.split("#")[0] + "#BudgetDocument-manage&/Details/" + Docnr + "/" + sYear + "/" + sFmArea;

                // sap.m.URLHelper.redirect(hrefNew, true);



                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "BudgetDocument-manage";
                var actionURL = "manage&/Details/" + Docnr + "/" + sYear + "/" + sFmArea;
                var semnticObj = "BudgetDocument";
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done(function (aResponses) {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: semnticObj,
                                    action: actionURL
                                }
                            })) || "";

                            var url = window.location.href.split('#')[0] + hash;
                            sap.m.URLHelper.redirect(url, true);
                        }

                    })
                    .fail(function () { });
            },
            selectRadioBtnShow: function (event) {

                owner.getModel("JSON").setProperty("/selectedFilters/requestType", '');
                owner.getModel("JSON").setProperty("/selectedFilters/project", '');
                owner.getModel("JSON").setProperty("/selectedFilters/projectValue", '');
                owner.getModel("JSON").setProperty("/selectedFilters/commitment", '');
                owner.getModel("JSON").setProperty("/selectedFilters/commitmentValue", '');

                oModels.loadProjects();
                oModels.loadCommitments();
            },
            onlyOpenedRequestsSelect: function (event) {
                var selected = event.getParameter("selected"),
                    model = owner.getModel("JSON");
                var userInfo = new sap.ushell.services.UserInfo().getUser();

                if (selected) {
                    model.setProperty("/selectedFilters/requestCreatedValue", userInfo.getFullName());
                    model.setProperty("/selectedFilters/requestCreated", userInfo.getId());
                } else {
                    model.setProperty("/selectedFilters/requestCreatedValue", "");
                    model.setProperty("/selectedFilters/requestCreated", "");
                }

            },
        });
    });

sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/Device"
], 
    /**
     * provide app-view type models (as in the first "V" in MVVC)
     * 
     * @param {typeof sap.ui.model.json.JSONModel} JSONModel
     * @param {typeof sap.ui.Device} Device
     * 
     * @returns {Function} createDeviceModel() for providing runtime info for the device the UI5 app is running on
     */
    function (JSONModel, Device) {
        "use strict";

        return {
            createDeviceModel: function () {
                var oModel = new JSONModel(Device);
                oModel.setDefaultBindingMode("OneWay");
                return oModel;
            },
    
            createJSONModel: function () {
                var oDateFrom = new Date(),
                    userInfo = new sap.ushell.services.UserInfo().getUser(),
                    currentDate = new Date(),
                
                    //Calculation 5 years back
                    currentYear = Number(currentDate.getFullYear()),
                    years = [{
                        year: (currentYear - 2).toString()
                    }, {
                        year: (currentYear - 1).toString()
                    }, {
                        year: currentYear.toString()
                    }, {
                        year: (currentYear + 1).toString()
                    }, {
                        year: (currentYear + 2).toString()
                    }];

                //one month ago
                // oDateFrom.setFullYear(new Date(new Date().setDate(new Date().getDate() - 30)).getFullYear());
                // oDateFrom.setMonth(new Date(new Date().setDate(new Date().getDate() - 30)).getMonth());
                // oDateFrom.setDate(new Date(new Date().setDate(new Date().getDate() - 30)).getDate());
                oDateFrom = new Date("01/01/" + oDateFrom.getFullYear().toString());

                var oModel = new sap.ui.model.json.JSONModel({
                    selectedFilters: {
                        productionDateFrom: oDateFrom,
                        productionDateTo: new Date(),
                        requestCreated: userInfo.getId(),
						requestCreatedValue: userInfo.getFullName(),
                        showBudgetIndex: "1",
						onlyOpened: true,
						requestType: "",
						requestStatus: "2",
						financialYear: "",
						teamKey: "",
						teamValue: "",
						project: "",
						projectValue: "",
						commitment: "",
						commitmentValue: "",
                        mahlakaValue: "",
                        stnValue: ""
                    },
                    NumberTypes: ["DocSum"],
                    NumberScala : {
                        "DocSum" : 2
                    },
                    financialYears: years,
                    reqTable: [],
                    Busy: false
    
                });
                oModel.setSizeLimit(10000);
                return oModel;
            },
    
            getReqTable: function (oFilters,resetUiTablefilter) {
                owner.getModel("JSON").setProperty("/Busy", true);
                owner.getModel("ODATA").read("/DocumentNumberSet", {
                    filters: oFilters,
                    success: function (oData, response) {
                        console.log(oData);
                        owner.getModel("JSON").setProperty("/Busy", false);
    
                        var aSonList = oData.results;
                        var aParent = [];
                        for (var i = 0; i < aSonList.length; i++) {
                            aSonList[i].iDocSum = parseFloat(aSonList[i].DocSum);

                            if(!aSonList[i].ParentDocnr && !aSonList[i].ParentFmArea) { //parent doc
                                aParent.push(aSonList[i]);
                                aSonList.splice(i, 1);
                                i--;
                            }
                        }
                        for(var i = 0; i < aSonList.length; i++){
                            for(var j = 0; j < aParent.length; j++){
                                if(aSonList[i].ParentDocnr === aParent[j].Docnr && aSonList[i].ParentFmArea === aParent[j].FmArea && aSonList[i].Docyear ===  aParent[j].Docyear){
                                    if(!aParent[j]['sons']){
                                        aParent[j]['sons'] = [];
                                    }
                                    aParent[j]['sons'].push(aSonList[i]);
                                    break;
                                }
                            }
                        }
                        owner.getModel("JSON").setProperty("/reqTable",aParent);

                        //set total sum rows
                        var iTotalItems = ("" + aParent.length).replace(/\B(?=(\d{3})+(?!\d))/g, ","),
                            sTitle = owner.getModel("i18n").getResourceBundle().getText("reqCount", [iTotalItems]);
                        owner.getModel("JSON").setProperty("/reqCount", sTitle);
                        
                        owner.getModel("JSON").refresh(false);

                    },
                    error: function (error) {
                    }
    
                });
    
            },
            
		loadProjects: function (ivFikrs) {
			var model = owner.getModel("JSON"),
				selFilters = model.getProperty("/selectedFilters"),
				requestProject = selFilters.showBudgetIndex,
				ivCourse = Number(selFilters.showBudgetIndex) + 1 ,
				EvFikrs = !!ivFikrs ? ivFikrs : model.getProperty("/evFikers"),
				oFilters = [new sap.ui.model.Filter("IvCourse", "EQ", ivCourse)];

			if (requestProject === '0') { //השקעות
				oFilters.push(new sap.ui.model.Filter("IvFikrs", "EQ", EvFikrs));
            }
			owner.getModel("ODATA").read("/FundCenterSet", { // ZUI_FM_GET_FUND_CENTER_LIST
				filters: oFilters,
				success: function (data) {
					console.log(data.results);
					var filterdArray = data.results.filter(function (ob) { // remove Course = 2. changed by roy 20.2
						//If it is not a duplicate, return true
						return ob.Course == 1;
					});
					model.setProperty("/projects", filterdArray);
				},
				error: function (error) {
				}
			});
		},
        loadCommitments: function (ivFikrs) {
			var model = owner.getModel("JSON"),
				selFilters = model.getProperty("/selectedFilters"),
				ivCourse = (Number(selFilters.showBudgetIndex) + 1),
				EvFikrs = !!ivFikrs ? ivFikrs : model.getProperty("/evFikers"),
				oFilters = [new sap.ui.model.Filter("IvCourse", "EQ", ivCourse), new sap.ui.model.Filter("IvFikrs", "EQ", EvFikrs)];

			owner.getModel("ODATA").read("/CommitmentItemSet", { // ZUI_FM_GET_COMMITMNT_ITEM_LIST
				filters: oFilters,
				success: function (data) {
					console.log(data.results);
					model.setProperty("/commitments", data.results);
				},
				error: function (error) {
					oModels.processError(error);
				}
			});
		},
        getUserArea: function () {
			return new Promise(function (resolve, reject) {
				var model = owner.getModel("JSON"),
					key = owner.getModel("CA").createKey("/GetUserAreaSet", { // ZUI_FM_CALC_RLSD_BUDGET_DATA
						IvUname: ""
					});
				owner.getModel("CA").read(key, {
					success: function (data) {
						model.setProperty("/evFikers", data.EvFikrs);
						model.setProperty("/evWerks", data.EvWerks); //user number
                        model.setProperty("/isSecurity", data.EvWerks === '1250'); 

						resolve(data);
					},
					error: function (error) {
						reject(error);
					}
				});
			});
		},
    };
});
/* global owner true */

sap.ui.define([
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"

], function (MessageBox, Filter, FilterOperator) {
	"use strict";
	return {

		setEscapeIgnoreDialog: function (dialog) {
			dialog.setEscapeHandler(
				function (oPromise) {
					oPromise.reject();
				}
			);
		},
		//-------------- P R O J E C T    S E A R C H    D I A L O G --------------//

		showProjectSearchDialog: function (event) {
			var dialog = owner.popover._getProjectSearchDialog();
			dialog.open();
		},

		_getProjectSearchDialog: function () {
			if (!owner._mainController._projectSearchDialog) {
				owner._mainController._projectSearchDialog = sap.ui.xmlfragment("ProjectSearchDialog",
					"alterno.zreqbudgetreport.view.fragments.ProjectSearchDialog", owner._mainController);
				owner._mainView.addDependent(owner._mainController._projectSearchDialog);

				owner.popover.setEscapeIgnoreDialog(owner._mainController._projectSearchDialog);
			}
			return owner._mainController._projectSearchDialog;
		},

		closeProjectSearchDialog: function (event) {
			owner.popover._getProjectSearchDialog().close();
		},

		beforeOpenSearchProjectDialog: function (event) {
			sap.ui.core.Fragment.byId("ProjectSearchDialog", "listProjectSearchDialog").getBinding("items").filter([]);
			sap.ui.core.Fragment.byId("ProjectSearchDialog", "searchFieldProject").setValue();
		},
		searchProject: function (event) {
			var query = event.getParameter("query"),
				model = owner.getModel("JSON"),
				oFilters = [];

			if (query) {
				var filters = [
					new sap.ui.model.Filter("Bezeich", "Contains", query),
					new sap.ui.model.Filter("Fictr", "Contains", query)
				];
				oFilters = new sap.ui.model.Filter(filters, false);
			}

			sap.ui.core.Fragment.byId("ProjectSearchDialog", "listProjectSearchDialog").getBinding("items").filter(oFilters);
		},

		selectProjectPress: function (event) {
			var object = event.getSource().getBindingContext("JSON").getObject(),
				model = owner.getModel("JSON");

			model.setProperty("/selectedFilters/projectValue", object.Bezeich);
			model.setProperty("/selectedFilters/project", object.Fictr);

			owner.popover.closeProjectSearchDialog();
		},
		//----------- C O M M I T M E N T    S E A R C H     D I A L O G ----------//

		showCommitmentSearchDialog: function (event) {
			var dialog = owner.popover._getCommitmentSearchDialog();
			dialog.open();
		},

		_getCommitmentSearchDialog: function () {
			if (!owner._mainController._commitmentSearchDialog) {
				owner._mainController._commitmentSearchDialog = sap.ui.xmlfragment("CommitmentSearchDialog",
					"alterno.zreqbudgetreport.view.fragments.CommitmentSearchDialog", owner._mainController);
				owner._mainView.addDependent(owner._mainController._commitmentSearchDialog);

				owner.popover.setEscapeIgnoreDialog(owner._mainController._commitmentSearchDialog);
			}
			return owner._mainController._commitmentSearchDialog;
		},

		closeCommitmentSearchDialog: function (event) {
			owner.popover._getCommitmentSearchDialog().close();
		},

		beforeOpenCommitmentProjectDialog: function (event) {
			sap.ui.core.Fragment.byId("CommitmentSearchDialog", "listCommitmentSearchDialog").getBinding("items").filter([]);
			sap.ui.core.Fragment.byId("CommitmentSearchDialog", "searchFieldCommitment").setValue();
		},
		searchCommitment: function (event) {
			var query = event.getParameter("query"),
				oFilters = [];

			if (query) {
				var filters = [
					new sap.ui.model.Filter("Bezei", "Contains", query),
					new sap.ui.model.Filter("Fipex", "Contains", query)
				];
				oFilters = new sap.ui.model.Filter(filters, false);
			}

			sap.ui.core.Fragment.byId("CommitmentSearchDialog", "listCommitmentSearchDialog").getBinding("items").filter(oFilters);
		},

		selectCommitmentPress: function (event) {
			var object = event.getSource().getBindingContext("JSON").getObject(),
				model = owner.getModel("JSON");

			model.setProperty("/selectedFilters/commitmentValue", object.Bezei);
			model.setProperty("/selectedFilters/commitment", object.Fipex);

			owner.popover.closeCommitmentSearchDialog();
		},
		//-------------- M A H L A K A    S E A R C H    D I A L O G --------------//

		showMahlakaSearchDialog: function (event) {
			var dialog = owner.popover._getMahlakaSearchDialog();
			dialog.open();
		},

		_getMahlakaSearchDialog: function () {
			if (!owner._mainController._mahlakaSearchDialog) {
				owner._mainController._mahlakaSearchDialog = sap.ui.xmlfragment("MahlakaSearchDialog",
					"alterno.zreqbudgetreport.view.fragments.MahlakaSearchDialog", owner._mainController);
				owner._mainView.addDependent(owner._mainController._mahlakaSearchDialog);

				owner.popover.setEscapeIgnoreDialog(owner._mainController._mahlakaSearchDialog);
			}
			return owner._mainController._mahlakaSearchDialog;
		},

		closeMahlakaSearchDialog: function (event) {
			owner.popover._getMahlakaSearchDialog().close();
		},

		beforeOpenSearchMahlakaDialog: function (event) {
			sap.ui.core.Fragment.byId("MahlakaSearchDialog", "listMahlakaSearchDialog").getBinding("items").filter([]);
			sap.ui.core.Fragment.byId("MahlakaSearchDialog", "searchFieldMahlaka").setValue();
		},

		searchMahlaka: function (event) {
			var query = event.getParameter("query").toLowerCase(),
				oFilters = [new Filter("Mahlaka", FilterOperator.Contains, query)];
			sap.ui.core.Fragment.byId("MahlakaSearchDialog", "listMahlakaSearchDialog").getBinding("items").filter(oFilters);
		},

		selectMahlakaPress: function (event) {
			var object = event.getSource().getBindingContext("ODATA").getObject(),
				model = owner.getModel("JSON");

			model.setProperty("/selectedFilters/mahlakaValue", object.Mahlaka);

			owner.popover.closeMahlakaSearchDialog();
		},
		//-------------- S T N    S E A R C H    D I A L O G --------------//

		showStnSearchDialog: function (event) {
			var dialog = owner.popover._getStnSearchDialog();
			dialog.open();
		},

		_getStnSearchDialog: function () {
			if (!owner._mainController._stnSearchDialog) {
				owner._mainController._stnSearchDialog = sap.ui.xmlfragment("StnSearchDialog",
					"alterno.zreqbudgetreport.view.fragments.StnSearchDialog", owner._mainController);
				owner._mainView.addDependent(owner._mainController._stnSearchDialog);

				owner.popover.setEscapeIgnoreDialog(owner._mainController._stnSearchDialog);
			}
			return owner._mainController._stnSearchDialog;
		},

		closeStnSearchDialog: function (event) {
			owner.popover._getStnSearchDialog().close();
		},

		beforeOpenSearchStnDialog: function (event) {
			sap.ui.core.Fragment.byId("StnSearchDialog", "listStnSearchDialog").getBinding("items").filter([]);
			sap.ui.core.Fragment.byId("StnSearchDialog", "searchFieldStn").setValue();
		},

		searchStn: function (event) {
			var query = event.getParameter("query"),
				oFilters = [new Filter(
					"Stn",
					FilterOperator.Contains,
					query.trim().toUpperCase(),
				)];
			sap.ui.core.Fragment.byId("StnSearchDialog", "listStnSearchDialog").getBinding("items").filter(oFilters);
		},

		selectStnPress: function (event) {
			var object = event.getSource().getBindingContext("ODATA").getObject(),
				model = owner.getModel("JSON");

			model.setProperty("/selectedFilters/stnValue", object.Stn);

			owner.popover.closeStnSearchDialog();
		},
		//-------------- C R E A T O R    S E A R C H    D I A L O G --------------//

		showCreatorSearchDialog: function (event) {
			var dialog = owner.popover._getCreatorSearchDialog();
			dialog.open();
		},

		_getCreatorSearchDialog: function () {
			if (!owner._mainController._creatorSearchDialog) {
				owner._mainController._creatorSearchDialog = sap.ui.xmlfragment("CreatorSearchDialog",
					"alterno.zreqbudgetreport.view.fragments.CreatorSearchDialog", owner._mainController);
				owner._mainView.addDependent(owner._mainController._creatorSearchDialog);

				owner.popover.setEscapeIgnoreDialog(owner._mainController._creatorSearchDialog);
			}
			return owner._mainController._creatorSearchDialog;
		},

		closeCreatorSearchDialog: function (event) {
			owner.popover._getCreatorSearchDialog().close();
		},

		beforeOpenSearchCreatorDialog: function (event) {
			sap.ui.core.Fragment.byId("CreatorSearchDialog", "listCreatorSearchDialog").getBinding("items").filter([]);
			sap.ui.core.Fragment.byId("CreatorSearchDialog", "searchFieldCreator").setValue();
		},

		searchCreator: function (event) {
			var query = event.getParameter("query").toLowerCase(),
				oFilters = [new Filter("IvFreeText", FilterOperator.EQ, query)];
			sap.ui.core.Fragment.byId("CreatorSearchDialog", "listCreatorSearchDialog").getBinding("items").filter(oFilters);
		},

		selectCreatorPress: function (event) {
			var object = event.getSource().getBindingContext("MASTERDATA").getObject(),
				model = owner.getModel("JSON");

			model.setProperty("/selectedFilters/requestCreatedValue", object.FullName);
			model.setProperty("/selectedFilters/requestCreated", object.Username);

			owner.popover.closeCreatorSearchDialog();
		},
		//----------- T E A M S    S E A R C H  N O T M U L T I  D I A L O G -----------//

		showNMultiTeamsSearchDialog: function (event) {
			var dialog = owner.popover._getNMultiTeamsSearchDialog();
			dialog.open();
		},

		_getNMultiTeamsSearchDialog: function () {
			if (!owner._mainController._nMultiteamsSearchDialog) {
				owner._mainController._nMultiteamsSearchDialog = sap.ui.xmlfragment("NMultiTeamsSearchDialog",
					"alterno.zreqbudgetreport.view.fragments.NMultiTeamsSearchDialog", owner._mainController);
				owner._mainView.addDependent(owner._mainController._nMultiteamsSearchDialog);

			}
			return owner._mainController._nMultiteamsSearchDialog;
		},

		closeNMultiTeamsSearchDialog: function (event) {
			owner.popover._getNMultiTeamsSearchDialog().close();
		},
		searchNMultiTeamsSh: function (event) {
			var query = event.getParameter("query"),
				aFilters = [],
				aQuery = query.split(" ");

			for (var i = 0; i < aQuery.length; i++) {
				if (!!aQuery[i]) {
					aFilters.push(new Filter({
						filters: [
							new Filter("Idnum", "Contains", aQuery[i]),
							new Filter("Stn", "Contains", aQuery[i]),
							new Filter("Mahlaka", "Contains", aQuery[i]),
							new Filter("Saif", "Contains", aQuery[i])

						],
						and: false
					}));
				}
			}

			sap.ui.core.Fragment.byId("NMultiTeamsSearchDialog", "listNMultiSearchDialog").getBinding("items").filter(aFilters);

		},

		onConfirmNMultiTeams: function (oEvent) {
			var object = oEvent.getSource().getBindingContext("ODATA").getObject(),
				model = owner.getModel("JSON");
				
			model.setProperty("/selectedFilters/teamKey", object.Idnum);
			owner.popover.closeNMultiTeamsSearchDialog();
		},
	};
});
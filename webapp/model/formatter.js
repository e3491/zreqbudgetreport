sap.ui.define([], function () {
	"use strict";
	return {
		getRequestStatus: function (statusCode) {
			if (statusCode === "1") {
				return owner.i18n("REGISTERED");
			} else if (statusCode === "2") {
				return owner.i18n("APPROVAL_ROUND");
			} else if (statusCode === "3") {
				return owner.i18n("APPROVAL_ENROLLED_CERTIFICATION");
			} else if (statusCode === "4") {
				return owner.i18n("REJECTED_CANCELED");
			} else {
				return "";
			}
		},
		isValidDate: function (date) {
			try {
				var currentTime = owner.formatter.normalizationTime(new Date()).getTime(),
					recievedTime = owner.formatter.normalizationTime(date).getTime();

				return recievedTime > currentTime;
			} catch (e) {}
		},
		normalizationTime: function (date) {
			if (!date)
				return date;

			date.setHours("03");
			date.setMinutes(0);
			date.setSeconds(0);

			return date;
		},
		getDate: function (value) {
			if (value) {
				var parts = value.split(".");
				return owner.formatter.normalizationTime(new Date(parts[1] + "/" + parts[0] + "/" + parts[2]));
			} else {
				return "";
			}
		}


	};
});
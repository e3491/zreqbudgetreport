var owner,
	oModels;

sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "alterno/zreqbudgetreport/model/models",
    "alterno/zreqbudgetreport/model/formatter",
    "alterno/zreqbudgetreport/model/popovers"
],
    function (UIComponent, Device, models, Formatters, Popovers) {
        "use strict";

        return UIComponent.extend("alterno.zreqbudgetreport.Component", {
            formatter: Formatters,
            popover: Popovers,

            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                owner = this;

                oModels = models;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                //Set Right to Left
                sap.ui.getCore().getConfiguration().setRTL(true);
                //Set HE language
                sap.ui.getCore().getConfiguration().setLanguage("iw_IL");

                // enable routing
                this.getRouter().initialize();

                // set the device model
                this.setModel(models.createDeviceModel(), "device");
             
                this.getModel("ODATA").setSizeLimit(1000);

                this.setModel(models.createJSONModel(), "JSON");
            },
            i18n: function (text) {
                return owner.getModel("i18n").getProperty(text);
            },
        });
    }
);
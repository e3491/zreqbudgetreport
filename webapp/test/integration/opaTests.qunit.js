/* global QUnit */

sap.ui.require(["alterno/zreqbudgetreport/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
